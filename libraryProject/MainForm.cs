﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace libraryProject
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void memberManagementButton_Click(object sender, EventArgs e)
        {
            MemberManagementForm memberForm = new MemberManagementForm();
            memberForm.Show();
        }

        private void BookManagementButton_Click(object sender, EventArgs e)
        {
            BookManagementForm bookForm = new BookManagementForm();
            bookForm.Show();
        }

       

        private void mainSearchButton_Click(object sender, EventArgs e)
        {

        }

        private void rentButton_Click(object sender, EventArgs e)
        {
            BookRentingForm bookRentForm = new BookRentingForm();
            bookRentForm.Show();
        }

       
    }
}
