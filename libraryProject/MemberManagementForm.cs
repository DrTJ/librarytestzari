﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace libraryProject
{
    public partial class MemberManagementForm : Form
    {
        public MemberManagementForm()
        {
            InitializeComponent();
        }

        private void MemberInsertButton_Click(object sender, EventArgs e)
        {
            MemberInsertForm memberInsertForm = new MemberInsertForm();
            memberInsertForm.Show();
        }

        private void memberDeleteButton_Click(object sender, EventArgs e)
        {
            MemberDeleteForm memberDeleteForm = new MemberDeleteForm();
            memberDeleteForm.Show();
        }

        private void memberEditButton_Click(object sender, EventArgs e)
        {
            MemberEditForm memberEditForm = new MemberEditForm();
            memberEditForm.Show();
        }

        private void memberActivationButton_Click(object sender, EventArgs e)
        {
            MemberActivationForm memberActivationForm = new MemberActivationForm();
            memberActivationForm.Show();
        }


        private void MemberMainSearchButton_Click(object sender, EventArgs e)
        {

            /*
            SqlConnection MemberMainSearchConnection = new SqlConnection();
            SqlCommand MemberMainSearchCommand = new SqlCommand();
            SqlDataAdapter MemberMainSearchAdapter = new SqlDataAdapter();
            SqlDataReader MemberMainSearchReader = new SqlDataReader();
            */
        }

        private void memberSearchButton_Click(object sender, EventArgs e)
        {
            MemberAdvancedSearchForm memberAdvancedSearchForm = new MemberAdvancedSearchForm();
            memberAdvancedSearchForm.Show();
        }


       
    }
}
