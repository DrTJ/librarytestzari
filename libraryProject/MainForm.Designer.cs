﻿namespace libraryProject
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mainSearchButton = new System.Windows.Forms.Button();
            this.rentButton = new System.Windows.Forms.Button();
            this.BookManagementButton = new System.Windows.Forms.Button();
            this.memberManagementButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.mainSearchButton);
            this.groupBox1.Controls.Add(this.rentButton);
            this.groupBox1.Controls.Add(this.BookManagementButton);
            this.groupBox1.Controls.Add(this.memberManagementButton);
            this.groupBox1.Location = new System.Drawing.Point(629, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(245, 290);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // mainSearchButton
            // 
            this.mainSearchButton.Location = new System.Drawing.Point(101, 154);
            this.mainSearchButton.Margin = new System.Windows.Forms.Padding(4);
            this.mainSearchButton.Name = "mainSearchButton";
            this.mainSearchButton.Size = new System.Drawing.Size(125, 32);
            this.mainSearchButton.TabIndex = 4;
            this.mainSearchButton.Text = "جستجو";
            this.mainSearchButton.UseVisualStyleBackColor = true;
            this.mainSearchButton.Click += new System.EventHandler(this.mainSearchButton_Click);
            // 
            // rentButton
            // 
            this.rentButton.Location = new System.Drawing.Point(101, 114);
            this.rentButton.Margin = new System.Windows.Forms.Padding(4);
            this.rentButton.Name = "rentButton";
            this.rentButton.Size = new System.Drawing.Size(125, 32);
            this.rentButton.TabIndex = 3;
            this.rentButton.Text = "امانتی ها";
            this.rentButton.UseVisualStyleBackColor = true;
            this.rentButton.Click += new System.EventHandler(this.rentButton_Click);
            // 
            // BookManagementButton
            // 
            this.BookManagementButton.Location = new System.Drawing.Point(101, 74);
            this.BookManagementButton.Margin = new System.Windows.Forms.Padding(4);
            this.BookManagementButton.Name = "BookManagementButton";
            this.BookManagementButton.Size = new System.Drawing.Size(125, 32);
            this.BookManagementButton.TabIndex = 2;
            this.BookManagementButton.Text = "مدیریت کتاب ها";
            this.BookManagementButton.UseVisualStyleBackColor = true;
            this.BookManagementButton.Click += new System.EventHandler(this.BookManagementButton_Click);
            // 
            // memberManagementButton
            // 
            this.memberManagementButton.Location = new System.Drawing.Point(101, 34);
            this.memberManagementButton.Margin = new System.Windows.Forms.Padding(4);
            this.memberManagementButton.Name = "memberManagementButton";
            this.memberManagementButton.Size = new System.Drawing.Size(125, 32);
            this.memberManagementButton.TabIndex = 1;
            this.memberManagementButton.Text = "مدیریت اعضا";
            this.memberManagementButton.UseVisualStyleBackColor = true;
            this.memberManagementButton.Click += new System.EventHandler(this.memberManagementButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Library Project";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button memberManagementButton;
        private System.Windows.Forms.Button mainSearchButton;
        private System.Windows.Forms.Button rentButton;
        private System.Windows.Forms.Button BookManagementButton;

    }
}

