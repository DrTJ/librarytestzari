﻿namespace libraryProject
{
    partial class BookManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bookReturnButton = new System.Windows.Forms.Button();
            this.bookRentingButton = new System.Windows.Forms.Button();
            this.bookEditButton = new System.Windows.Forms.Button();
            this.bookSearchButton = new System.Windows.Forms.Button();
            this.bookDeleteButton = new System.Windows.Forms.Button();
            this.bookInsertButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bookReturnButton);
            this.groupBox1.Controls.Add(this.bookRentingButton);
            this.groupBox1.Controls.Add(this.bookEditButton);
            this.groupBox1.Controls.Add(this.bookSearchButton);
            this.groupBox1.Controls.Add(this.bookDeleteButton);
            this.groupBox1.Controls.Add(this.bookInsertButton);
            this.groupBox1.Location = new System.Drawing.Point(556, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(903, 590);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // bookReturnButton
            // 
            this.bookReturnButton.Location = new System.Drawing.Point(142, 240);
            this.bookReturnButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bookReturnButton.Name = "bookReturnButton";
            this.bookReturnButton.Size = new System.Drawing.Size(135, 30);
            this.bookReturnButton.TabIndex = 6;
            this.bookReturnButton.Text = "بازگشت کتاب";
            this.bookReturnButton.UseVisualStyleBackColor = true;
            // 
            // bookRentingButton
            // 
            this.bookRentingButton.Location = new System.Drawing.Point(142, 200);
            this.bookRentingButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bookRentingButton.Name = "bookRentingButton";
            this.bookRentingButton.Size = new System.Drawing.Size(135, 30);
            this.bookRentingButton.TabIndex = 5;
            this.bookRentingButton.Text = "کرایه کتاب";
            this.bookRentingButton.UseVisualStyleBackColor = true;
            // 
            // bookEditButton
            // 
            this.bookEditButton.Location = new System.Drawing.Point(142, 160);
            this.bookEditButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bookEditButton.Name = "bookEditButton";
            this.bookEditButton.Size = new System.Drawing.Size(135, 30);
            this.bookEditButton.TabIndex = 4;
            this.bookEditButton.Text = "ویرایش کتاب";
            this.bookEditButton.UseVisualStyleBackColor = true;
            // 
            // bookSearchButton
            // 
            this.bookSearchButton.Location = new System.Drawing.Point(142, 120);
            this.bookSearchButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bookSearchButton.Name = "bookSearchButton";
            this.bookSearchButton.Size = new System.Drawing.Size(135, 30);
            this.bookSearchButton.TabIndex = 3;
            this.bookSearchButton.Text = "جستجو کتاب";
            this.bookSearchButton.UseVisualStyleBackColor = true;
            // 
            // bookDeleteButton
            // 
            this.bookDeleteButton.Location = new System.Drawing.Point(142, 80);
            this.bookDeleteButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bookDeleteButton.Name = "bookDeleteButton";
            this.bookDeleteButton.Size = new System.Drawing.Size(135, 30);
            this.bookDeleteButton.TabIndex = 2;
            this.bookDeleteButton.Text = "حذف کتاب";
            this.bookDeleteButton.UseVisualStyleBackColor = true;
            // 
            // bookInsertButton
            // 
            this.bookInsertButton.Location = new System.Drawing.Point(142, 40);
            this.bookInsertButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bookInsertButton.Name = "bookInsertButton";
            this.bookInsertButton.Size = new System.Drawing.Size(135, 30);
            this.bookInsertButton.TabIndex = 1;
            this.bookInsertButton.Text = "ثبت کتاب";
            this.bookInsertButton.UseVisualStyleBackColor = true;
            this.bookInsertButton.Click += new System.EventHandler(this.bookInsertButton_Click);
            // 
            // BookManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "BookManagementForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BookManagementForm";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bookReturnButton;
        private System.Windows.Forms.Button bookRentingButton;
        private System.Windows.Forms.Button bookEditButton;
        private System.Windows.Forms.Button bookSearchButton;
        private System.Windows.Forms.Button bookDeleteButton;
        private System.Windows.Forms.Button bookInsertButton;
    }
}