﻿namespace libraryProject
{
    partial class MemberManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MemberMainSearchButton = new System.Windows.Forms.Button();
            this.memberActivationButton = new System.Windows.Forms.Button();
            this.memberSearchButton = new System.Windows.Forms.Button();
            this.memberEditButton = new System.Windows.Forms.Button();
            this.memberDeleteButton = new System.Windows.Forms.Button();
            this.MemberInsertButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MemberMainSearchButton);
            this.groupBox1.Controls.Add(this.memberActivationButton);
            this.groupBox1.Controls.Add(this.memberSearchButton);
            this.groupBox1.Controls.Add(this.memberEditButton);
            this.groupBox1.Controls.Add(this.memberDeleteButton);
            this.groupBox1.Controls.Add(this.MemberInsertButton);
            this.groupBox1.Location = new System.Drawing.Point(595, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(261, 281);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // MemberMainSearchButton
            // 
            this.MemberMainSearchButton.Location = new System.Drawing.Point(66, 238);
            this.MemberMainSearchButton.Name = "MemberMainSearchButton";
            this.MemberMainSearchButton.Size = new System.Drawing.Size(165, 34);
            this.MemberMainSearchButton.TabIndex = 6;
            this.MemberMainSearchButton.Text = "جستجوی کلی";
            this.MemberMainSearchButton.UseVisualStyleBackColor = true;
            this.MemberMainSearchButton.Click += new System.EventHandler(this.MemberMainSearchButton_Click);
            // 
            // memberActivationButton
            // 
            this.memberActivationButton.Location = new System.Drawing.Point(66, 198);
            this.memberActivationButton.Name = "memberActivationButton";
            this.memberActivationButton.Size = new System.Drawing.Size(165, 34);
            this.memberActivationButton.TabIndex = 5;
            this.memberActivationButton.Text = "فعال و غیرفعال کردن اعضا";
            this.memberActivationButton.UseVisualStyleBackColor = true;
            this.memberActivationButton.Click += new System.EventHandler(this.memberActivationButton_Click);
            // 
            // memberSearchButton
            // 
            this.memberSearchButton.Location = new System.Drawing.Point(66, 159);
            this.memberSearchButton.Name = "memberSearchButton";
            this.memberSearchButton.Size = new System.Drawing.Size(165, 33);
            this.memberSearchButton.TabIndex = 4;
            this.memberSearchButton.Text = "جستجوی پیشرفته ";
            this.memberSearchButton.UseVisualStyleBackColor = true;
            this.memberSearchButton.Click += new System.EventHandler(this.memberSearchButton_Click);
            // 
            // memberEditButton
            // 
            this.memberEditButton.Location = new System.Drawing.Point(66, 120);
            this.memberEditButton.Name = "memberEditButton";
            this.memberEditButton.Size = new System.Drawing.Size(165, 33);
            this.memberEditButton.TabIndex = 3;
            this.memberEditButton.Text = "ویرایش اعضا";
            this.memberEditButton.UseVisualStyleBackColor = true;
            this.memberEditButton.Click += new System.EventHandler(this.memberEditButton_Click);
            // 
            // memberDeleteButton
            // 
            this.memberDeleteButton.Location = new System.Drawing.Point(66, 82);
            this.memberDeleteButton.Name = "memberDeleteButton";
            this.memberDeleteButton.Size = new System.Drawing.Size(165, 33);
            this.memberDeleteButton.TabIndex = 2;
            this.memberDeleteButton.Text = "حذف اعضا";
            this.memberDeleteButton.UseVisualStyleBackColor = true;
            this.memberDeleteButton.Click += new System.EventHandler(this.memberDeleteButton_Click);
            // 
            // MemberInsertButton
            // 
            this.MemberInsertButton.Location = new System.Drawing.Point(66, 43);
            this.MemberInsertButton.Name = "MemberInsertButton";
            this.MemberInsertButton.Size = new System.Drawing.Size(165, 33);
            this.MemberInsertButton.TabIndex = 1;
            this.MemberInsertButton.Text = "ثبت اعضا";
            this.MemberInsertButton.UseVisualStyleBackColor = true;
            this.MemberInsertButton.Click += new System.EventHandler(this.MemberInsertButton_Click);
            // 
            // MemberManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MemberManagementForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MemberManagementForm";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button memberSearchButton;
        private System.Windows.Forms.Button memberEditButton;
        private System.Windows.Forms.Button memberDeleteButton;
        private System.Windows.Forms.Button MemberInsertButton;
        private System.Windows.Forms.Button memberActivationButton;
        private System.Windows.Forms.Button MemberMainSearchButton;
    }
}