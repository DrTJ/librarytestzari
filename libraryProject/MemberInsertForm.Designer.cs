﻿namespace libraryProject
{
    partial class MemberInsertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MemberId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberShipId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreatedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreatedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdatedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdatedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.memberInsertButton = new System.Windows.Forms.Button();
            this.memberUpdatedBy = new System.Windows.Forms.TextBox();
            this.memberUpdatedDate = new System.Windows.Forms.DateTimePicker();
            this.memberCreateByTextBox = new System.Windows.Forms.TextBox();
            this.memeberCreatedDate = new System.Windows.Forms.DateTimePicker();
            this.memberActiveStatusCB = new System.Windows.Forms.ComboBox();
            this.memberAddressTextBox = new System.Windows.Forms.TextBox();
            this.memberEmailTextBox = new System.Windows.Forms.TextBox();
            this.memberMobileTextBox = new System.Windows.Forms.TextBox();
            this.memberPhoneTextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.memberShipTextbox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.memberPersonalPic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberPersonalPic)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Linen;
            this.dataGridView1.ColumnHeadersHeight = 54;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId,
            this.MemberShipId,
            this.FirstName,
            this.LastName,
            this.Phone,
            this.Mobile,
            this.Email,
            this.Address,
            this.Status,
            this.CreatedDate,
            this.CreatedBy,
            this.UpdatedDate,
            this.UpdatedBy});
            this.dataGridView1.Location = new System.Drawing.Point(23, 307);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Nazanin", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 30;
            this.dataGridView1.Size = new System.Drawing.Size(1298, 288);
            this.dataGridView1.TabIndex = 61;
            // 
            // MemberId
            // 
            this.MemberId.DataPropertyName = "MemberId ";
            this.MemberId.HeaderText = "کد عضو";
            this.MemberId.Name = "MemberId";
            this.MemberId.ReadOnly = true;
            // 
            // MemberShipId
            // 
            this.MemberShipId.DataPropertyName = "MemberShipId";
            this.MemberShipId.HeaderText = "کد عضویت";
            this.MemberShipId.Name = "MemberShipId";
            this.MemberShipId.ReadOnly = true;
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "نام";
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            // 
            // LastName
            // 
            this.LastName.DataPropertyName = "LastName";
            this.LastName.HeaderText = "نام خانوادگی";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            // 
            // Phone
            // 
            this.Phone.DataPropertyName = "PhoneNumber";
            this.Phone.HeaderText = "تلفن";
            this.Phone.Name = "Phone";
            this.Phone.ReadOnly = true;
            // 
            // Mobile
            // 
            this.Mobile.DataPropertyName = "Mobile";
            this.Mobile.HeaderText = "موبایل";
            this.Mobile.Name = "Mobile";
            this.Mobile.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "ایمیل";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // Address
            // 
            this.Address.DataPropertyName = "Address";
            this.Address.HeaderText = "آدرس";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "ActivationStatus";
            this.Status.HeaderText = "وضعیت";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // CreatedDate
            // 
            this.CreatedDate.DataPropertyName = "Created";
            this.CreatedDate.HeaderText = "ثبت شده در تاریخ";
            this.CreatedDate.Name = "CreatedDate";
            this.CreatedDate.ReadOnly = true;
            // 
            // CreatedBy
            // 
            this.CreatedBy.DataPropertyName = "CreatedBy";
            this.CreatedBy.HeaderText = "ثبت شده توسط";
            this.CreatedBy.Name = "CreatedBy";
            this.CreatedBy.ReadOnly = true;
            // 
            // UpdatedDate
            // 
            this.UpdatedDate.DataPropertyName = "Updated";
            this.UpdatedDate.HeaderText = "بروز رسانی شده در تاریخ";
            this.UpdatedDate.Name = "UpdatedDate";
            this.UpdatedDate.ReadOnly = true;
            // 
            // UpdatedBy
            // 
            this.UpdatedBy.DataPropertyName = "UpdatedBy";
            this.UpdatedBy.HeaderText = "بروز رسانی شده توسط";
            this.UpdatedBy.Name = "UpdatedBy";
            this.UpdatedBy.ReadOnly = true;
            // 
            // memberInsertButton
            // 
            this.memberInsertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberInsertButton.Location = new System.Drawing.Point(389, 225);
            this.memberInsertButton.Name = "memberInsertButton";
            this.memberInsertButton.Size = new System.Drawing.Size(234, 40);
            this.memberInsertButton.TabIndex = 60;
            this.memberInsertButton.Text = "ثبت عضو";
            this.memberInsertButton.UseVisualStyleBackColor = true;
            this.memberInsertButton.Click += new System.EventHandler(this.memberInsertButton_Click);
            // 
            // memberUpdatedBy
            // 
            this.memberUpdatedBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberUpdatedBy.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberUpdatedBy.Location = new System.Drawing.Point(339, 161);
            this.memberUpdatedBy.Name = "memberUpdatedBy";
            this.memberUpdatedBy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.memberUpdatedBy.Size = new System.Drawing.Size(200, 27);
            this.memberUpdatedBy.TabIndex = 59;
            // 
            // memberUpdatedDate
            // 
            this.memberUpdatedDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberUpdatedDate.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberUpdatedDate.Location = new System.Drawing.Point(339, 125);
            this.memberUpdatedDate.Name = "memberUpdatedDate";
            this.memberUpdatedDate.Size = new System.Drawing.Size(200, 27);
            this.memberUpdatedDate.TabIndex = 58;
            // 
            // memberCreateByTextBox
            // 
            this.memberCreateByTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberCreateByTextBox.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberCreateByTextBox.Location = new System.Drawing.Point(339, 92);
            this.memberCreateByTextBox.Name = "memberCreateByTextBox";
            this.memberCreateByTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.memberCreateByTextBox.Size = new System.Drawing.Size(200, 27);
            this.memberCreateByTextBox.TabIndex = 57;
            // 
            // memeberCreatedDate
            // 
            this.memeberCreatedDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memeberCreatedDate.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memeberCreatedDate.Location = new System.Drawing.Point(339, 56);
            this.memeberCreatedDate.Name = "memeberCreatedDate";
            this.memeberCreatedDate.Size = new System.Drawing.Size(200, 27);
            this.memeberCreatedDate.TabIndex = 56;
            // 
            // memberActiveStatusCB
            // 
            this.memberActiveStatusCB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberActiveStatusCB.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberActiveStatusCB.FormattingEnabled = true;
            this.memberActiveStatusCB.Items.AddRange(new object[] {
            "فعال",
            "غیرفعال"});
            this.memberActiveStatusCB.Location = new System.Drawing.Point(339, 22);
            this.memberActiveStatusCB.Name = "memberActiveStatusCB";
            this.memberActiveStatusCB.Size = new System.Drawing.Size(200, 28);
            this.memberActiveStatusCB.TabIndex = 55;
            // 
            // memberAddressTextBox
            // 
            this.memberAddressTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberAddressTextBox.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.memberAddressTextBox.Location = new System.Drawing.Point(784, 224);
            this.memberAddressTextBox.Multiline = true;
            this.memberAddressTextBox.Name = "memberAddressTextBox";
            this.memberAddressTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.memberAddressTextBox.Size = new System.Drawing.Size(420, 68);
            this.memberAddressTextBox.TabIndex = 54;
            this.memberAddressTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // memberEmailTextBox
            // 
            this.memberEmailTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberEmailTextBox.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberEmailTextBox.Location = new System.Drawing.Point(899, 191);
            this.memberEmailTextBox.Name = "memberEmailTextBox";
            this.memberEmailTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.memberEmailTextBox.Size = new System.Drawing.Size(305, 27);
            this.memberEmailTextBox.TabIndex = 53;
            this.memberEmailTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // memberMobileTextBox
            // 
            this.memberMobileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberMobileTextBox.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberMobileTextBox.Location = new System.Drawing.Point(1004, 158);
            this.memberMobileTextBox.Name = "memberMobileTextBox";
            this.memberMobileTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.memberMobileTextBox.Size = new System.Drawing.Size(200, 27);
            this.memberMobileTextBox.TabIndex = 52;
            this.memberMobileTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // memberPhoneTextBox
            // 
            this.memberPhoneTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberPhoneTextBox.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberPhoneTextBox.Location = new System.Drawing.Point(1004, 125);
            this.memberPhoneTextBox.Name = "memberPhoneTextBox";
            this.memberPhoneTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.memberPhoneTextBox.Size = new System.Drawing.Size(200, 27);
            this.memberPhoneTextBox.TabIndex = 51;
            this.memberPhoneTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lastNameTextBox.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.lastNameTextBox.Location = new System.Drawing.Point(1004, 92);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lastNameTextBox.Size = new System.Drawing.Size(200, 27);
            this.lastNameTextBox.TabIndex = 50;
            this.lastNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.firstNameTextBox.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.firstNameTextBox.Location = new System.Drawing.Point(1004, 59);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.firstNameTextBox.Size = new System.Drawing.Size(200, 27);
            this.firstNameTextBox.TabIndex = 49;
            this.firstNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // memberShipTextbox
            // 
            this.memberShipTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memberShipTextbox.Font = new System.Drawing.Font("B Nazanin", 10F);
            this.memberShipTextbox.Location = new System.Drawing.Point(1004, 26);
            this.memberShipTextbox.Name = "memberShipTextbox";
            this.memberShipTextbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.memberShipTextbox.Size = new System.Drawing.Size(200, 27);
            this.memberShipTextbox.TabIndex = 48;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(592, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 23);
            this.label12.TabIndex = 47;
            this.label12.Text = "ثبت شده در تاریخ";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(552, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 23);
            this.label11.TabIndex = 46;
            this.label11.Text = "به روز رسانی شده در تاریخ";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(563, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 23);
            this.label10.TabIndex = 45;
            this.label10.Text = "به روز رسانی شده توسط";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(603, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 23);
            this.label9.TabIndex = 44;
            this.label9.Text = "ثبت شده توسط";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(643, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 23);
            this.label8.TabIndex = 43;
            this.label8.Text = "وضعیت";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1270, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 23);
            this.label7.TabIndex = 42;
            this.label7.Text = "آدرس";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1228, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 23);
            this.label6.TabIndex = 41;
            this.label6.Text = "پست الکترونیک";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1270, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 23);
            this.label5.TabIndex = 40;
            this.label5.Text = "موبایل";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1257, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 23);
            this.label4.TabIndex = 39;
            this.label4.Text = "تلفن ثابت";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1242, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 23);
            this.label3.TabIndex = 38;
            this.label3.Text = "نام خانوادگی";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1287, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 23);
            this.label2.TabIndex = 37;
            this.label2.Text = "نام";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1250, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 23);
            this.label1.TabIndex = 36;
            this.label1.Text = "کد عضویت";
            // 
            // memberPersonalPic
            // 
            this.memberPersonalPic.Location = new System.Drawing.Point(66, 35);
            this.memberPersonalPic.Name = "memberPersonalPic";
            this.memberPersonalPic.Size = new System.Drawing.Size(174, 225);
            this.memberPersonalPic.TabIndex = 62;
            this.memberPersonalPic.TabStop = false;
            // 
            // MemberInsertForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1345, 607);
            this.Controls.Add(this.memberPersonalPic);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.memberInsertButton);
            this.Controls.Add(this.memberUpdatedBy);
            this.Controls.Add(this.memberUpdatedDate);
            this.Controls.Add(this.memberCreateByTextBox);
            this.Controls.Add(this.memeberCreatedDate);
            this.Controls.Add(this.memberActiveStatusCB);
            this.Controls.Add(this.memberAddressTextBox);
            this.Controls.Add(this.memberEmailTextBox);
            this.Controls.Add(this.memberMobileTextBox);
            this.Controls.Add(this.memberPhoneTextBox);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.memberShipTextbox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("B Nazanin", 11.25F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MemberInsertForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MemberInsertForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberPersonalPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberShipId;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mobile;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdatedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdatedBy;
        private System.Windows.Forms.Button memberInsertButton;
        private System.Windows.Forms.TextBox memberUpdatedBy;
        private System.Windows.Forms.DateTimePicker memberUpdatedDate;
        private System.Windows.Forms.TextBox memberCreateByTextBox;
        private System.Windows.Forms.DateTimePicker memeberCreatedDate;
        private System.Windows.Forms.ComboBox memberActiveStatusCB;
        private System.Windows.Forms.TextBox memberAddressTextBox;
        private System.Windows.Forms.TextBox memberEmailTextBox;
        private System.Windows.Forms.TextBox memberMobileTextBox;
        private System.Windows.Forms.TextBox memberPhoneTextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.TextBox memberShipTextbox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox memberPersonalPic;

    }
}